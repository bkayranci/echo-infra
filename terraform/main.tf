provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_kubernetes_cluster" "echo-k8s" {
  name   = "echo-k8s"
  region = var.k8s_region
  version = var.k8s_version

  node_pool {
    name       = "autoscale-worker-pool"
    size       = "s-4vcpu-8gb"
    auto_scale = true
    min_nodes  = 1
    max_nodes  = 5
  }
}


provider "gitlab" {
  token = var.gitlab_access_token
}

resource "gitlab_project_variable" "kubeconfig" {
  project         = data.gitlab_project.echo_helm_project.id
  key             = "KUBECONFIG"
  value           = digitalocean_kubernetes_cluster.echo-k8s.kube_config.0.raw_config
  variable_type   = "file"
  protected       = true
  masked          = false
}

resource "gitlab_project_variable" "echo_version_variable" {
  project         = data.gitlab_project.echo_helm_project.id
  key             = "ECHO_VERSION"
  value           = var.echo_version
  protected       = true
}
