output "k8s_endpoint" {
    value = digitalocean_kubernetes_cluster.echo-k8s.endpoint
}

output "k8s_cluster_ca_certificate" {
    value = digitalocean_kubernetes_cluster.echo-k8s.kube_config.0.cluster_ca_certificate
    sensitive = true
}

output "k8s_client_key" {
    value = digitalocean_kubernetes_cluster.echo-k8s.kube_config.0.client_key
    sensitive = true
}

output "k8s_client_certificate" {
    value = digitalocean_kubernetes_cluster.echo-k8s.kube_config.0.client_certificate
    sensitive = true
}

output "k8s_token" {
    value = digitalocean_kubernetes_cluster.echo-k8s.kube_config.0.token
    sensitive = true
}