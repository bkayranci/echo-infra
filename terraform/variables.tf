variable "do_token" {
  type = string
}

variable "gitlab_access_token" {
  type = string
}

variable "k8s_version" {
  type    = string
  default = "1.20.7-do.0"
}

variable "k8s_region" {
  type    = string
  default = "nyc1"
}

variable "echo_version" {
  type    = string
  default = "latest"
}